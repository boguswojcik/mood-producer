include github.com/msales/make/golang


run:
	go generate ./ && go run ./main.go
.PHONY: run

consume:
	@docker-compose exec -T kafka \
		kafka-console-consumer.sh \
		--bootstrap-server localhost:9092 \
		--from-beginning \
		--topic=events
.PHONY: consume