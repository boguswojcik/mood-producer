package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/boguswojcik/mood-producer/mood"
	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
)

//go:generate protoc -I ./vendor/bitbucket.org/boguswojcik/mood-schema/ ./vendor/bitbucket.org/boguswojcik/mood-schema/mood.proto --go_out=./mood/

func main() {

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	cfg := sarama.NewConfig()

	cfg.Producer.Return.Successes = true

	producer, err := sarama.NewSyncProducer([]string{"localhost:9092"}, cfg)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Mood producer started.")

	for {
		select {
		case <-sigs:
			fmt.Println("Mood producer exited.")
			os.Exit(0)

		default:
			time.Sleep(time.Duration(2) * time.Second)

			err = producer.SendMessages([]*sarama.ProducerMessage{
				getHappyMessage(),
				getSadMessage(),
			})

			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("Produced happy and sad message.")
		}
	}
}

var happyMoods = []string{
	"content",
	"cheerful",
	"delighted",
	"ecstatic",
	"pleased",
}

func getHappyMessage() *sarama.ProducerMessage {
	event := &mood.Event{
		Type:         mood.EventType_HAPPINESS,
		MoodName:     happyMoods[rand.Intn(len(happyMoods))],
		MoodStrenght: int32(rand.Intn(10)),
	}

	b, err := proto.Marshal(event)
	if err != nil {
		log.Fatal(err)
	}

	msg := &sarama.ProducerMessage{
		Topic: "events",
		Value: sarama.ByteEncoder(b),
	}

	return msg
}

var sadMoods = []string{
	"bitter",
	"dismal",
	"heartbroken",
	"melancholy",
	"mournful",
	"pessimistic",
}

func getSadMessage() *sarama.ProducerMessage {
	event := &mood.Event{
		Type:         mood.EventType_SADNESS,
		MoodName:     sadMoods[rand.Intn(len(sadMoods))],
		MoodStrenght: int32(rand.Intn(10)),
	}

	b, err := proto.Marshal(event)
	if err != nil {
		log.Fatal(err)
	}

	msg := &sarama.ProducerMessage{
		Topic: "events",
		Value: sarama.ByteEncoder(b),
	}

	return msg
}
